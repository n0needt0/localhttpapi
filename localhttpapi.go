/**
Copyright 2015 andrew@yasinsky.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package localhttpapi

import (
	"bitbucket.org/n0needt0/uppr"
	"encoding/json"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/pat"
	"github.com/stretchr/objx"
	"io"
	"net/http"
	_ "net/http/pprof"
	"os"
	"time"
)

type ApiEnv struct {
	Objx *objx.Map
}

var log = uppr.GetLog()

func New(Objx *objx.Map) {
	api := &ApiEnv{Objx: Objx}
	//get log handle function from config
	var loghandle func(http.ResponseWriter, *http.Request)
	ok := false
	if loghandle, ok = Objx.Get("loghandle").Data().(func(http.ResponseWriter, *http.Request)); !ok {
		log.Fatalf("can not get loghandle!")
	}
	//get address and port
	var address string
	ok = false
	if address, ok = Objx.Get("localhttp").Data().(string); !ok {
		log.Fatalf("can not get localhttp!")
	}

	r := pat.New()
	r.Get("/health", http.HandlerFunc(api.GetHealth))
	r.Get("/loglevel", http.HandlerFunc(loghandle))
	http.Handle("/", r)
	log.Noticef("HTTP Listening %s", address)
	err := http.ListenAndServe(address, handlers.LoggingHandler(os.Stdout, r))
	if err != nil {
		log.Fatalf("ListenAndServe: %s", err.Error())
	}
}

func (api *ApiEnv) GetHealth(w http.ResponseWriter, r *http.Request) {
	res := make(map[string]interface{})
	res["ts"] = time.Now().Format("2006-01-02 15:04:01 PST")
	res["config"] = fmt.Sprintf("%+v", api.Objx)
	res["stats"] = api.Objx.Get("stats")
	who := fmt.Sprintf("%s", os.Args[0])
	res["who"] = who
	b, err := json.Marshal(res)
	if err != nil {
		log.Errorf("error: %s", err)
	}
	io.WriteString(w, string(b[:])+"\n")
	return
}
